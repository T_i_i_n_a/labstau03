package lesson1;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

public class LoginTests {

    @Test
    public void loginWebexInChrome() {
        WebDriver chromeWebDriver = new ChromeDriver();
        chromeWebDriver.navigate()
                .to("https://dtag.webex.com/webappng/sites/dtag/dashboard?siteurl=dtag");
        chromeWebDriver.quit();
    }

    @Test
    public void loginWebexInFirefox() {
        WebDriver firefoxWebDriver = new FirefoxDriver();
        firefoxWebDriver.navigate()
                .to("https://dtag.webex.com/webappng/sites/dtag/dashboard?siteurl=dtag");
        firefoxWebDriver.quit();
    }

    @Test
    public void loginWebexInEdge() {
        WebDriver edgeWebDriver = new EdgeDriver();
        edgeWebDriver.navigate()
                .to("https://dtag.webex.com/webappng/sites/dtag/dashboard?siteurl=dtag");
        edgeWebDriver.quit();
    }
}
