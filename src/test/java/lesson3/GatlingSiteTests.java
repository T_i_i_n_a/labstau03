package lesson3;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;


@Test
public class GatlingSiteTests {

    @BeforeMethod
    public void openGatlingSite() {
        open("https://gatling.io/");
    }

    @Test
    public void checkNavigationButtonsTitles() {
        openPricingPage();

        checkCloudButtonTitle();
        checkSelfHostedButtonTitle();
        checkMarketplacesButtonTitle();
    }

    @Test
    public void checkCarouselContainerColumnsTitlesAreDisplayed() {
        openPricingPage();

        checkScoutColumnTitleIsDisplayed();
        checkScaleColumnTitleIsDisplayed();
        checkCorporateColumnTitleIsDisplayed();
    }

    @Test
    public void checkWrapperElements() {
        openPricingPage();

        checkHeaderElement();
        checkTextElement();
    }

    private void openPricingPage() {
        $(By.xpath("//ul[@id='menu-navbar']//a[contains(text(), 'Pricing')]"))
                .click();
    }

    private void checkCloudButtonTitle() {
        $(By.xpath("//a[@href='#cloud']"))
                .shouldBe(Condition.visible)
                .shouldHave(Condition.text("Cloud"));
    }

    private void checkSelfHostedButtonTitle() {
        $(By.xpath("//a[@href='#self_hosted']"))
                .shouldBe(Condition.visible)
                .shouldHave(Condition.text("Self-hosted"));
    }

    private void checkMarketplacesButtonTitle() {
        $(By.xpath("//a[@href='#Marketplaces-tab']"))
                .shouldBe(Condition.visible)
                .shouldHave(Condition.text("Marketplaces"));
    }

    private void checkScoutColumnTitleIsDisplayed() {
        $(By.xpath("//h3[contains(text(), 'Scout')]"))
                .shouldBe(Condition.visible);
    }

    private void checkScaleColumnTitleIsDisplayed() {
        $(By.xpath("//h3[contains(text(), 'Scale')]"))
                .shouldBe(Condition.visible);
    }

    private void checkCorporateColumnTitleIsDisplayed() {
        $(By.xpath("//h3[contains(text(), 'Corporate')]"))
                .shouldBe(Condition.visible);
    }

    private void checkHeaderElement() {
        $(By.xpath("//div[@id='cloud']//div[@class='wpb_wrapper']//div[@class='fo__content ']//h3"))
                .shouldBe(Condition.visible)
                .shouldHave(Condition.text("Cloud plans"));
    }

    private void checkTextElement() {
        $(By.xpath("//div[@id='cloud']//div[@class='wpb_wrapper']//div[@class='fo__content ']//p"))
                .shouldBe(Condition.visible)
                .shouldHave(Condition.text("Test Gatling Cloud for free ! No credit card required."));
    }
}
