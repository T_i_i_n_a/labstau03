package lesson2;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


@Test
public class GatlingSiteTests {

    WebDriver chromeWebDriver;

    @BeforeMethod
    public void openGatlingSite() {
        chromeWebDriver = new ChromeDriver();
        chromeWebDriver.manage().window().setSize(new Dimension(1920, 1080));
        chromeWebDriver.navigate().to("https://gatling.io/");
    }

    @AfterMethod
    public void closeBrowser() {
        chromeWebDriver.quit();
    }

    @Test
    public void checkNavigationButtonsTitles() {
        openPricingPage();

        checkCloudButtonTitle();
        checkSelfHostedButtonTitle();
        checkMarketplacesButtonTitle();
    }

    @Test
    public void checkCarouselContainerColumnsTitlesAreDisplayed() {
        openPricingPage();

        checkScoutColumnTitleIsDisplayed();
        checkScaleColumnTitleIsDisplayed();
        checkCorporateColumnTitleIsDisplayed();
    }

    @Test
    public void checkWrapperElements() {
        openPricingPage();

        checkHeaderElement();
        checkTextElement();
    }

    private void openPricingPage() {
        chromeWebDriver
                .findElement(By.xpath("//ul[@id='menu-navbar']//a[contains(text(), 'Pricing')]"))
                .click();
    }

    private void checkCloudButtonTitle() {
        WebElement cloudButton = chromeWebDriver
                .findElement(By.xpath("//ul[@class='nav-tabs']//a[@href='#cloud']"));

        Assert.assertTrue(cloudButton.isDisplayed(), "Cloud button isn't displayed.");
        Assert.assertEquals(cloudButton.getText().trim(), "Cloud",
                "Cloud button text differs from expected");
    }

    private void checkSelfHostedButtonTitle() {
        WebElement selfHostedButton = chromeWebDriver
                .findElement(By.xpath("//ul[@class='nav-tabs']//a[@href='#self_hosted']"));

        Assert
                .assertTrue(selfHostedButton.isDisplayed(), "Self-hosted button isn't displayed.");
        Assert
                .assertEquals(selfHostedButton.getText().trim(), "Self-hosted",
                        "Self-hosted button text differs from expected");
    }

    private void checkMarketplacesButtonTitle() {
        WebElement marketplacesButton = chromeWebDriver
                .findElement(By.xpath("//ul[@class='nav-tabs']//a[@href='#Marketplaces-tab']"));

        Assert.assertTrue(marketplacesButton.isDisplayed(), "Marketplaces button isn't displayed.");
        Assert.assertEquals(marketplacesButton.getText().trim(), "Marketplaces",
                "Marketplaces button text differs from expected");
    }

    private void checkScoutColumnTitleIsDisplayed() {
        WebElement scoutColumnTitle = chromeWebDriver
                .findElement(By.xpath("//h3[contains(text(), 'Scout')]"));

        Assert.assertTrue(scoutColumnTitle.isDisplayed(), "Scout column title isn't displayed.");
    }

    private void checkScaleColumnTitleIsDisplayed() {
        WebElement scaleColumnTitle = chromeWebDriver
                .findElement(By.xpath("//h3[contains(text(), 'Scale')]"));

        Assert.assertTrue(scaleColumnTitle.isDisplayed(), "Scale column title isn't displayed.");
    }

    private void checkCorporateColumnTitleIsDisplayed() {
        WebElement corporateColumnTitle = chromeWebDriver
                .findElement(By.xpath("//h3[contains(text(), 'Corporate')]"));

        Assert.assertTrue(corporateColumnTitle.isDisplayed(), "Corporate column title isn't displayed.");
    }

    private void checkHeaderElement() {
        WebElement header = chromeWebDriver
                .findElement(By.xpath("//div[@id='cloud']//div[@class='wpb_wrapper']//div[@class='fo__content ']//h3"));

        Assert.assertTrue(header.isDisplayed(), "Wrapper header isn't displayed.");
        Assert.assertEquals(header.getText().trim(), "Cloud plans",
                "Wrapper header text differs from expected");

    }

    private void checkTextElement() {
        WebElement text = chromeWebDriver
                .findElement(By.xpath("//div[@id='cloud']//div[@class='wpb_wrapper']//div[@class='fo__content ']//p"));

        Assert.assertTrue(text.isDisplayed(), "Wrapper text isn't displayed.");
        Assert.assertEquals(text.getText().trim(), "Test Gatling Cloud for free ! No credit card required.",
                "Wrapper text text differs from expected");
    }
}
