Feature: check WLAN Comfort tariff page on telekom.de web-site

  Scenario: check WLAN Comfort tariff page on telekom.de web-site
    Given open telekom.de web-page
    And select menu "DSL & Glasfaser"
    And click menu item "WLAN & Router"
    When select product "WLAN Comfort"
    Then check opened page header is "WLAN Comfort"
    And check button "BESTELLEN" is displayed and enabled