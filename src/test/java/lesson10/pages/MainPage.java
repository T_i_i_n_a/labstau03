package lesson10.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import static com.codeborne.selenide.Selenide.page;

public class MainPage {

    @FindBy(how = How.ID, using = "consentAcceptAll")
    private SelenideElement acceptAllButton;

    @FindBy(how = How.XPATH, using = "//a[@title='\"DSL & Glasfaser\"']")
    private SelenideElement dslAndGlasfaserMenuTitle;

    @FindBy(how = How.XPATH, using = "//a[@title='\"WLAN & Router\"']")
    private SelenideElement wlanAndRouterMenuItem;


    public void clickAcceptAllButton() {
        acceptAllButton.click();
    }

    public void selectMenu(String menuTitleValue) {
        switch (menuTitleValue) {
            case "\"DSL & Glasfaser\"": {
                dslAndGlasfaserMenuTitle.hover();
            }
        }
    }

    public WlanAndRouterPage clickMenuItem(String menuItemTitleValue) {
        switch (menuItemTitleValue) {
            case "\"WLAN & Router\"": {
                wlanAndRouterMenuItem.click();
            }
        }

        return page(WlanAndRouterPage.class);
    }
}
