package lesson10.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import static com.codeborne.selenide.Selenide.page;

public class WlanAndRouterPage {

    @FindBy(how = How.XPATH, using = "//strong[contains(text(), '\"WLAN Comfort\"')]")
    SelenideElement wlanComfortTitle;

    public WlanComfortPage clickMenuItem(String menuItemTitleValue) {
        switch (menuItemTitleValue) {
            case "\"WLAN Comfort\"": {
                wlanComfortTitle.click();
            }
        }

        return page(WlanComfortPage.class);
    }
}
