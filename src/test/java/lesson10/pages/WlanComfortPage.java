package lesson10.pages;

import com.codeborne.selenide.SelenideElement;
import io.cucumber.java.en.And;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class WlanComfortPage {

    @FindBy(how = How.XPATH, using = "//h1[contains(text(), '\"WLAN Comfort\"')]")
    SelenideElement wlanComfortPageHeader;

    @FindBy(how = How.XPATH, using = "//a[contains(text(), '\"Bestellen\"')]")
    SelenideElement bestellenButton;

    public void checkOpenedPageHeaderIsDisplayed(String pageHeaderValue) {
        switch (pageHeaderValue) {
            case "\"WLAN Comfort\"": {
                wlanComfortPageHeader.isDisplayed();
            }
        }
    }


    @And("check button {string} is displayed and enabled")
    public void checkButtonIsDisplayedAndEnabled(String buttonTextValue) {
        switch (buttonTextValue) {
            case "\"BESTELLEN\"": {
                bestellenButton.isDisplayed();
                bestellenButton.isEnabled();
            }
        }
    }
}
