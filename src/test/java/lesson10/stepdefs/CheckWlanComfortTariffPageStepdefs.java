package lesson10.stepdefs;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lesson10.pages.MainPage;
import lesson10.pages.WlanAndRouterPage;
import lesson10.pages.WlanComfortPage;

import static com.codeborne.selenide.Selenide.open;

public class CheckWlanComfortTariffPageStepdefs {
    MainPage mainPage;
    WlanAndRouterPage wlanAndRouterPage;
    WlanComfortPage wlanComfortPage;

    @Given("open telekom.de web-page")
    public void openTelekomDeWebPage() {
        mainPage = open("https://www.telekom.de/", MainPage.class);
        mainPage.clickAcceptAllButton();
    }

    @And("select menu {string}")
    public void selectMenu(String menuTitleValue) {
        mainPage.selectMenu(menuTitleValue);
    }

    @And("click menu item {string}")
    public void clickMenuItem(String menuItemTitleValue) {
        wlanAndRouterPage = mainPage.clickMenuItem(menuItemTitleValue);
    }

    @When("select product {string}")
    public void selectProductWithTitle(String productTitleValue) {
        wlanComfortPage = wlanAndRouterPage.clickMenuItem(productTitleValue);
    }

    @Then("check opened page header is {string}")
    public void checkOpenedPageHeaderIsDisplayed(String pageHeaderValue) {
        wlanComfortPage.checkOpenedPageHeaderIsDisplayed(pageHeaderValue);
    }

    @And("check button {string} is displayed and enabled")
    public void checkButton(String buttonTextValue) {
        wlanComfortPage.checkButtonIsDisplayedAndEnabled(buttonTextValue);
    }
}
