Feature: practice 1 test on telekom.de web-site

  Scenario: check some elements as practice 1 on telekom.de web-site
    Given open telekom.de web-page
    When click Mobilfunk menu item
    Then check Mobilfunk page URL
    And check icons count on Mobilfunk page

    When click Geraete icon
    Then check Geraete page URL
    And check icons count on Geraete page

    When click Smartwatches icon
    And click "TCL Safety Watch MT43AX" card
    Then check Einmalige Zahlung price value is "229 €"





