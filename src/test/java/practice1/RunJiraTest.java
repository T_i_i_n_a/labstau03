package practice1;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/practice1/resources/jira",
        glue = "practice1/stepdefs",
        plugin = {"json:target/cucumber-html-reports/cucumber.json", "pretty"}
)
public class RunJiraTest {}
