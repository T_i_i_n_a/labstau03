package practice1;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/practice1/features/get_features_jira_xray.feature",
        glue = "practice1/stepdefs",
        plugin = "pretty"
)

public class JiraExportTest {}
