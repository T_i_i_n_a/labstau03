package practice1;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/practice1/features/post_results_jira_xray.feature",
        glue = "practice1/stepdefs",
        plugin = "pretty"
)

public class JiraImportTest {}
