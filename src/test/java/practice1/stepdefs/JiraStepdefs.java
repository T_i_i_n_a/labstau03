package practice1.stepdefs;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import utils.jira_xray.XRayCucumberFeatureFetcher;
import utils.jira_xray.XRayCucumberResultsImporter;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class JiraStepdefs {
    List<String> ids = new ArrayList<>();
    File outputDir;
    File resultsFile;

    @Given("feature fetch parameters are defined")
    public void featureFetchParametersAreDefined() {
        ids.add("TAAADEV-1112");
        outputDir = new File("src/test/java/practice1/resources/jira");
    }

    @When("a request to jira x-ray is sent")
    public void aRequestToJiraXRayIsSent() {
        XRayCucumberFeatureFetcher featureFetcher = new XRayCucumberFeatureFetcher();
        featureFetcher.fetch(ids, outputDir);
    }

    @Then("feature file is received")
    public void featureFileIsReceived() throws IOException {
        Assert.assertTrue(
                "No files received",
                Files.list(Paths.get("src/test/java/practice1/resources/jira"))
                        .findAny().isPresent()
        );
    }

    @Given("test result file is present")
    public void testResultFileIsPresent() throws IOException {
        Assert.assertTrue(
                "No files received",
                Files.list(Paths.get("target/cucumber-html-reports"))
                        .findAny().isPresent()
        );
    }

    @When("test result file to jira x-ray is sent")
    public void testResultFileToJiraXRayIsSent() {
        resultsFile = new File("target/cucumber-html-reports/cucumber.json");
        XRayCucumberResultsImporter resultsImporter = new XRayCucumberResultsImporter();
        resultsImporter.importResults(resultsFile);
    }

    @Then("results are successfully imported")
    public void resultsAreSuccessfullyImported() {}
}
