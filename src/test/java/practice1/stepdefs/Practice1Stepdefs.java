package practice1.stepdefs;

import com.codeborne.selenide.Condition;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;

import static com.codeborne.selenide.CollectionCondition.size;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverConditions.url;

public class Practice1Stepdefs {
    @Given("open telekom.de web-page")
    public void openTelekomDeWebPage() {
        open("https://www.telekom.de/");
        $(By.id("consentAcceptAll")).click();
    }

    @When("click Mobilfunk menu item")
    public void clickMobilfunkMenuItem() {
        $(By.xpath("//a[@href='/unterwegs']")).click();
    }

    @Then("check Mobilfunk page URL")
    public void checkMobilfunkPageURL() {
        webdriver()
                .shouldHave(url("https://www.telekom.de/unterwegs"));
    }

    @And("check icons count on Mobilfunk page")
    public void checkIconsCountOnMobilfunkPage() {
        $$(By.xpath("//li[@class='nav__item']"))
                .shouldHave(size(6));
    }

    @When("click Geraete icon")
    public void clickGeraeteIcon() {
        $(By.xpath("//a[@href='/unterwegs/smartphones-und-tablets']"))
                .click();
    }

    @Then("check Geraete page URL")
    public void checkGeraetePageURL() {
        webdriver()
                .shouldHave(url("https://www.telekom.de/unterwegs/smartphones-und-tablets"));
    }

    @And("check icons count on Geraete page")
    public void checkIconsCountOnGeraetePage() {
        $$(By.xpath("//li[@class='category-nav__item category-nav__item--round']"))
                .shouldHave(size(5));
    }

    @When("click Smartwatches icon")
    public void clickSmartwatchesIcon() {
        $(By.xpath("//a[@href='/mobilfunk/geraete/smartwatch']"))
                .click();
    }

    @And("click {string} card")
    public void click(String watchesModel) {
        String xpath = "//span[text()='" + watchesModel + "']";
        $(By.xpath(xpath)).click();
    }

    @Then("check Einmalige Zahlung price value is {string}")
    public void checkEinmaligeZahlungPriceValueIs(String price) {
        $(By.xpath("//p[text()='Einmalige Zahlung']/following-sibling::p/span[@data-qa='label_price']"))
                .shouldHave(Condition.text(price));
    }
}
