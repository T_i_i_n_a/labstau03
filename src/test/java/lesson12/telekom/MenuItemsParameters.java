package lesson12.telekom;

public enum MenuItemsParameters {

    MOBILFUNK(Constants.MOBILFUNK_CONST, "Mobilfunk", true),
    DSL_AND_GLASFASER(Constants.DSL_AND_GLASFASER_CONST, "DSL & Glasfaser", true),
    TV(Constants.TV_CONST, "TV", true),
    MAGENTA_EINS(Constants.MAGENTA_EINS_CONST, "MagentaEINS", false),
    SMARTE_PRODUKTE(Constants.SMARTE_PRODUKTE_CONST, "Smarte Produkte", true),
    NETZ(Constants.NETZ_CONST, "Netz", true),
    SERVICE(Constants.SERVICE_CONST, "Service", false);


    private final String url;
    private final String text;
    private final boolean isDropdownMenu;


    MenuItemsParameters(String title, String text, boolean isDropdownMenu) {
        this.url = title;
        this.text = text;
        this.isDropdownMenu = isDropdownMenu;
    }

    public String getUrl() {
        return this.url;
    }

    public String getText() {
        return this.text;
    }

    public boolean isDropdownMenu() {
        return this.isDropdownMenu;
    }
}
