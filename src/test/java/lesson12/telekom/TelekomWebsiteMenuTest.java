package lesson12.telekom;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.Duration;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class TelekomWebsiteMenuTest {

    @BeforeMethod
    public void openTelekomSite() {
        open("https://www.telekom.de/start");
        $(By.id("consentAcceptAll")).should(Condition.appear, Duration.ofSeconds(25));
        $(By.id("consentAcceptAll")).click();
    }

    @Test
    public void checkMobilfunkMenuItem() {
        for (MenuItemsParameters menuItem : MenuItemsParameters.values()) {
            checkMenuItem(menuItem);
        }
    }


    private void checkMenuItem(MenuItemsParameters parameters) {
        String xpath = "//a[@href='" + parameters.getUrl() + "']";

        $(By.xpath(xpath)).shouldHave(Condition.text(parameters.getText()));

        if (parameters.isDropdownMenu()) {
            $(By.xpath(xpath)).hover();
            switch (parameters.getUrl()) {
                case (Constants.MOBILFUNK_CONST): {
                    $(By.xpath("//a[@href='/unterwegs/tarife-und-optionen']")).shouldBe(Condition.visible);
                    break;
                }
                case (Constants.DSL_AND_GLASFASER_CONST): {
                    $(By.xpath("//a[@href='/festnetz/tarife-und-optionen']")).shouldBe(Condition.visible);
                    break;
                }
                case (Constants.TV_CONST): {
                    $(By.xpath("//a[@href='/magenta-tv/tarife-und-optionen']")).shouldBe(Condition.visible);
                    break;
                }
                case (Constants.SMARTE_PRODUKTE_CONST): {
                    $(By.xpath("//a[@href='/smarte-produkte/iot']")).shouldBe(Condition.visible);
                    break;
                }
                case (Constants.NETZ_CONST): {
                    $(By.xpath("//a[@href='/netz/5g']")).shouldBe(Condition.visible);
                    break;
                }
                case (Constants.SERVICE_CONST): {
                    $(By.xpath("//a[@href='/hilfe']")).shouldBe(Condition.visible);
                    break;
                }
                default:
                    break;
            }
        }
    }
}
