package lesson12.telekom;

public class Constants {
    public static final String MOBILFUNK_CONST = "/unterwegs";
    public static final String DSL_AND_GLASFASER_CONST = "/festnetz";
    public static final String TV_CONST = "/magenta-tv";
    public static final String MAGENTA_EINS_CONST = "/magenta-eins";
    public static final String SMARTE_PRODUKTE_CONST = "/smarte-produkte";
    public static final String NETZ_CONST = "/netz";
    public static final String SERVICE_CONST = "/hilfe/magentaservice";
}
