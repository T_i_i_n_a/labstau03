package lesson12.postman;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import utils.properties.PropertyLoader;

import java.time.Duration;

import static com.codeborne.selenide.Selenide.*;

public class PostmanLoginTest {

    @BeforeMethod
    public void openPostmanSite() {
        open("https://www.postman.com/");
    }

    @Test
    public void loginToPostmanSite() {
        clickLoginButton();
        enterUsername();
        enterPassword();
        clickSignInButton();
        waitPageForLoggedUserAppears();
        checkPageForLoggedUserIsOpened();
    }

    @AfterMethod
    public void logoutFromPostmanSite() {
        openUserInfoDropdown();
        clickSignOutItem();
        clickSignOutButtonOnPopUp();
        waitPageForLoginAppears();
    }

    private void waitPageForLoginAppears() {
        $(By.xpath("//div[@class='pm-container-fluid']")).should(Condition.appear, Duration.ofSeconds(25));
    }

    private void clickSignOutButtonOnPopUp() {
        $(By.xpath("//div[contains(text(), 'Sign Out')]")).click();
    }

    private void clickSignOutItem() {
        $(By.xpath("//div[contains(text(), 'Sign Out')]")).click();
    }

    private void openUserInfoDropdown() {
        $(By.xpath("//div[@class='dropdown user-info-dropdown']//div[@class='dropdown-button']")).click();
    }


    private void checkPageForLoggedUserIsOpened() {
        $(By.id("app-root")).shouldBe(Condition.visible);
    }

    private void waitPageForLoggedUserAppears() {
        $(By.id("app-root")).should(Condition.appear, Duration.ofSeconds(25));
    }

    private void clickSignInButton() {
        $(By.id("sign-in-btn")).click();
    }

    private void enterPassword() {
        $(By.id("password")).sendKeys(PropertyLoader.loadProperty("password"));
    }

    private void enterUsername() {
        $(By.id("username")).sendKeys(PropertyLoader.loadProperty("login"));
    }

    private void clickLoginButton() {
        $$(By.xpath("//button[@data-testid='aether-button']//span[contains(text(), 'Sign In')]")).first().click();
    }
}
