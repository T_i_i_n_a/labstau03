package lesson5.stepdefs;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class CheckWlanComfortTariffPageStepdefs {
    @Given("open telekom.de web-page")
    public void openTelekomDeWebPage() {
        open("https://www.telekom.de/");
        $(By.id("consentAcceptAll")).click();
    }

    @And("select menu {string}")
    public void selectMenu(String menuTitleValue) {
        String xpath = "//a[@title='" + menuTitleValue + "']";
        $(By.xpath(xpath)).hover();
    }

    @And("click menu item {string}")
    public void clickMenuItem(String menuItemTitleValue) {
        String xpath = "//a[@title='" + menuItemTitleValue + "']";
        $(By.xpath(xpath)).click();
    }

    @When("select product {string}")
    public void selectProductWithTitle(String productTitleValue) {
        String xpath = "//strong[contains(text(), '" + productTitleValue + "')]";
        $(By.xpath(xpath)).click();
    }

    @Then("check opened page header is {string}")
    public void checkOpenedPageHeaderIsDisplayed(String pageHeaderValue) {
        String xpath = "//h1[contains(text(), '" + pageHeaderValue + "')]";
        $(By.xpath(xpath)).isDisplayed();
    }

    @And("check button {string} is displayed and enabled")
    public void checkButton(String buttonTextValue) {
        String xpath = "//a[contains(text(), '" + convertButtonText(buttonTextValue) + "')]";
        $(By.xpath(xpath)).isDisplayed();
        $(By.xpath(xpath)).isEnabled();
    }

    private String convertButtonText(String word) {
        return word.substring(0, 1).toUpperCase() + word.substring(1).toLowerCase();
    }
}
