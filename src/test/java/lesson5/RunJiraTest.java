package lesson5;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/lesson5/resources/jira",
        glue = "lesson5/stepdefs",
        plugin = {"json:target/cucumber-html-reports/cucumber.json", "pretty", "io.qameta.allure.cucumber7jvm.AllureCucumber7Jvm" }
)
public class RunJiraTest {}
