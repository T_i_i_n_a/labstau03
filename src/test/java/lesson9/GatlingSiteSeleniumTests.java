package lesson9;

import io.qameta.allure.Description;
import io.qameta.allure.Step;
import org.monte.screenrecorder.ScreenRecorder;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import utils.attachments.Screenshots;
import utils.listeners.TestRunListener;

import java.awt.*;
import java.io.IOException;

@Listeners(TestRunListener.class)
@Test
public class GatlingSiteSeleniumTests {

    WebDriver chromeWebDriver;

    @BeforeMethod
    public void openGatlingSite(ITestContext iTestContext) {
        chromeWebDriver = new ChromeDriver();
        iTestContext.setAttribute("driver", chromeWebDriver);
        chromeWebDriver.manage().window().setSize(new Dimension(1920, 1080));
        chromeWebDriver.navigate().to("https://gatling.io/");
    }

    @AfterMethod
    public void closeBrowser() {
        chromeWebDriver.quit();
    }

    @Test
    @Description("Check navigation buttons titles")
    public void checkNavigationButtonsTitles() throws IOException {

//        Screenshots.takeScreenshot(chromeWebDriver, "./target/screenshots/");

        openPricingPage();

        checkCloudButtonTitle();
        checkSelfHostedButtonTitle();
        checkMarketplacesButtonTitle();
    }

    @Test
    public void checkCarouselContainerColumnsTitlesAreDisplayed() {
        openPricingPage();

        checkScoutColumnTitleIsDisplayed();
        checkScaleColumnTitleIsDisplayed();
        checkCorporateColumnTitleIsDisplayed();
    }

    @Test
    public void checkWrapperElements() throws IOException, AWTException {
//        GraphicsConfiguration gc =
//                GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration();
//        ScreenRecorder screenRecorder = new ScreenRecorder(gc);
//        screenRecorder.start();

        openPricingPage();

        checkHeaderElement();
        checkTextElement();

//        screenRecorder.stop();
    }

    @Step("Open pricing page")
    private void openPricingPage() {
        chromeWebDriver
                .findElement(By.xpath("//ul[@id='menu-navbar']//a[contains(text(), 'Pricing')]"))
                .click();
    }

    @Step("Check Cloud button title")
    private void checkCloudButtonTitle() {
        WebElement cloudButton = chromeWebDriver
                .findElement(By.xpath("//ul[@class='nav-tabs']//a[@href='#cloud']"));

        Assert.assertTrue(cloudButton.isDisplayed(), "Cloud button isn't displayed.");
        Assert.assertEquals(cloudButton.getText().trim(), "Cloud",
                "Cloud button text differs from expected");
    }

    @Step("Check Self hosted button title")
    private void checkSelfHostedButtonTitle() {
        WebElement selfHostedButton = chromeWebDriver
                .findElement(By.xpath("//ul[@class='nav-tabs']//a[@href='#self_hosted']"));

        Assert
                .assertTrue(selfHostedButton.isDisplayed(), "Self-hosted button isn't displayed.");
        Assert
                .assertEquals(selfHostedButton.getText().trim(), "Self-hosted",
                        "Self-hosted button text differs from expected");
    }

    @Step("Check Marketplaces button title")
    private void checkMarketplacesButtonTitle() {
        WebElement marketplacesButton = chromeWebDriver
                .findElement(By.xpath("//ul[@class='nav-tabs']//a[@href='#Marketplaces-tab']"));

        Assert.assertTrue(marketplacesButton.isDisplayed(), "Marketplaces button isn't displayed.");
        Assert.assertEquals(marketplacesButton.getText().trim(), "Marketplaces",
                "Marketplaces button text differs from expected");
    }

    @Step("Check Scout column title is displayed")
    private void checkScoutColumnTitleIsDisplayed() {
        WebElement scoutColumnTitle = chromeWebDriver
                .findElement(By.xpath("//h3[contains(text(), 'Scout')]"));

        Assert.assertTrue(scoutColumnTitle.isDisplayed(), "Scout column title isn't displayed.");
    }

    @Step("Check Scale column title is displayed")
    private void checkScaleColumnTitleIsDisplayed() {
        WebElement scaleColumnTitle = chromeWebDriver
                .findElement(By.xpath("//h3[contains(text(), 'Scale')]"));

        Assert.assertTrue(scaleColumnTitle.isDisplayed(), "Scale column title isn't displayed.");
    }

    @Step("Check Corporate column title is displayed")
    private void checkCorporateColumnTitleIsDisplayed() {
        WebElement corporateColumnTitle = chromeWebDriver
                .findElement(By.xpath("//h3[contains(text(), 'Corporate')]"));

        Assert.assertTrue(corporateColumnTitle.isDisplayed(), "Corporate column title isn't displayed.");
    }

    @Step("Check header element")
    private void checkHeaderElement() {
        WebElement header = chromeWebDriver
                .findElement(By.xpath("//div[@id='cloud']//div[@class='wpb_wrapper']//div[@class='fo__content ']//h3"));

        Assert.assertTrue(header.isDisplayed(), "Wrapper header isn't displayed.");
        Assert.assertEquals(header.getText().trim(), "Cloud plans",
                "Wrapper header text differs from expected");

    }

    @Step("Check text element")
    private void checkTextElement() {
        WebElement text = chromeWebDriver
                .findElement(By.xpath("//div[@id='cloud']//div[@class='wpb_wrapper']//div[@class='fo__content ']//p"));

        Assert.assertTrue(text.isDisplayed(), "Wrapper text isn't displayed.");
        Assert.assertEquals(text.getText().trim(), "Test Gatling Cloud for free ! No credit card required. 12345",
                "Wrapper text text differs from expected");
    }
}
