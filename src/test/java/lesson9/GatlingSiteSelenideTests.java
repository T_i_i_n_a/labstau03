package lesson9;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.Step;
import io.qameta.allure.selenide.AllureSelenide;
import org.openqa.selenium.By;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;


@Test
public class GatlingSiteSelenideTests {

    @BeforeMethod
    public void openGatlingSite() {
        SelenideLogger.addListener(
                "AllureSelenide",
                new AllureSelenide().screenshots(true).savePageSource(false).includeSelenideSteps(true)
        );
        open("https://gatling.io/");
    }

    @Test
    public void checkNavigationButtonsTitles() {
        openPricingPage();

        checkCloudButtonTitle();
        checkSelfHostedButtonTitle();
        checkMarketplacesButtonTitle();
    }

    @Test
    public void checkCarouselContainerColumnsTitlesAreDisplayed() {
        openPricingPage();

        checkScoutColumnTitleIsDisplayed();
        checkScaleColumnTitleIsDisplayed();
        checkCorporateColumnTitleIsDisplayed();
    }

    @Test
    public void checkWrapperElements() {
        openPricingPage();

        checkHeaderElement();
        checkTextElement();
    }

    @Step("Open pricing page.")
    private void openPricingPage() {
        $(By.xpath("//ul[@id='menu-navbar']//a[contains(text(), 'Pricing')]"))
                .click();
    }

    @Step("Check Cloud button title")
    private void checkCloudButtonTitle() {
        $(By.xpath("//a[@href='#cloud']"))
                .shouldBe(Condition.visible)
                .shouldHave(Condition.text("Cloud"));
    }

    @Step("Check Self Hosted button title")
    private void checkSelfHostedButtonTitle() {
        $(By.xpath("//a[@href='#self_hosted']"))
                .shouldBe(Condition.visible)
                .shouldHave(Condition.text("Self-hosted"));
    }

    @Step("Check Marketplaces button title")
    private void checkMarketplacesButtonTitle() {
        $(By.xpath("//a[@href='#Marketplaces-tab']"))
                .shouldBe(Condition.visible)
                .shouldHave(Condition.text("Marketplaces"));
    }

    @Step("Check Scout column title is displayed")
    private void checkScoutColumnTitleIsDisplayed() {
        $(By.xpath("//h3[contains(text(), 'Scout')]"))
                .shouldBe(Condition.visible);
    }

    @Step("Check Scale column title is displayed")
    private void checkScaleColumnTitleIsDisplayed() {
        $(By.xpath("//h3[contains(text(), 'Scale')]"))
                .shouldBe(Condition.visible);
    }

    @Step("Check Corporate column title is displayed")
    private void checkCorporateColumnTitleIsDisplayed() {
        $(By.xpath("//h3[contains(text(), 'Corporate')]"))
                .shouldBe(Condition.visible);
    }

    @Step("Check header element")
    private void checkHeaderElement() {
        $(By.xpath("//div[@id='cloud']//div[@class='wpb_wrapper']//div[@class='fo__content ']//h3"))
                .shouldBe(Condition.visible)
                .shouldHave(Condition.text("Cloud plans"));
    }

    @Step("Check text element")
    private void checkTextElement() {
        $(By.xpath("//div[@id='cloud']//div[@class='wpb_wrapper']//div[@class='fo__content ']//p"))
                .shouldBe(Condition.visible)
                .shouldHave(Condition.text("Test Gatling Cloud for free ! No credit card required."));
    }
}
